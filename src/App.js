import './App.sass';
import {useEffect, useState} from "react";
import alphabet from "./alphabet";


const rand = () => Math.floor(Math.random() * 100000);
const uniqueRand = (n,i) => {
    const b = [];
    while (b.length !== n) {
        const r = rand() % i
        if (!b.includes(r))
            b.push(r)
    }
    return b;
}

function shuffle(array) {
    let currentIndex = array.length, randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex > 0) {

        // Pick a remaining element.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }

    return array;
}

function App() {

    const [letter, setLetter] = useState('')
    const [answers, setAnswers] = useState([])
    const [correct, setCorrect] = useState('')
    const [answered, setAnswered] = useState(false)

    const gen = () => {
        let pairs = Object.entries(alphabet)
        let ur = uniqueRand(4,pairs.length)
        let selected = pairs[ur.pop() ]

        let wrong = ur.map(x => pairs[x][1])


        setLetter(selected[0])
        setAnswers(shuffle([selected[1], ...wrong]))
        setCorrect(selected[1])

    }

    useEffect(() => {
        gen()
    }, []);
    return (
        <div className="App">
            <div className={"Center"}>
                <h1 className={'Question'}>{letter}</h1>
                <div className={'AnswersRow'}>
                    {answers.map(x => <button
                                              className={`Button ${(answered === x ? (answered === correct ? 'correct' : 'wrong') : '')} ${answered && x === correct ? 'correct' : ''}`}
                                              onClick={() => {
                                                  if (answered) {
                                                      setAnswered(false)
                                                      gen()
                                                  } else
                                                      setAnswered(x)
                                              }}
                    >{x}</button>)}
                </div>
            </div>
        </div>
    );
}

export default App;
